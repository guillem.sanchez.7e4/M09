<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La meva calculadora</title>
</head>
<body>

<?php
$resultado = "";

// Obtener los valores del formulario
$num1 = isset($_POST['num1']) ? $_POST['num1'] : 0;
$num2 = isset($_POST['num2']) ? $_POST['num2'] : 0;
$operacion = isset($_POST['operacion']) ? $_POST['operacion'] : '';

// Realizar operaciones
if ($operacion == 'suma') {
    $resultado = "$num1 + $num2 = " . ($num1 + $num2) . "<br>";
} elseif ($operacion == 'resta') {
    $resultado = "$num1 - $num2 = " . ($num1 - $num2) . "<br>";
} elseif ($operacion == 'multiplicacion') {
    $resultado = "$num1 * $num2 = " . ($num1 * $num2) . "<br>";
} elseif ($operacion == 'division') {
    // Verificar la división por cero
    $resultado = "$num1 / $num2 = " . (($num2 != 0) ? ($num1 / $num2) : "No se puede dividir por cero") . "<br>";
} else {
    $resultado = ""; // Dejar $resultado vacío en caso de operación no válida
}
?>

<h1>La meva calculadora</h1>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <label for="num1">Número 1:</label>
    <input type="text" id="num1" name="num1" required>

    <br>

    <label for="num2">Número 2:</label>
    <input type="text" id="num2" name="num2" required>

    <br>

    <label for="operacion">Operación:</label>
    <select id="operacion" name="operacion" required>
        <option value="suma">Suma</option>
        <option value="resta">Resta</option>
        <option value="multiplicacion">Multiplicación</option>
        <option value="division">División</option>
    </select>

    <br>

    <button type="submit">Calcular</button>
</form>

<?php
// Mostrar el resultado directamente, incluso si es "Operación no válida"
echo $resultado;
?>

</body>
</html>
