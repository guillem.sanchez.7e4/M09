<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Calculadora</title>
</head>
<body>

<?php
// Inicializar la variable de resultados
$resultados = "";
// Inicializar la variable para mostrar o no los resultados
$mostrarResultados = false;

// Verificar si se ha enviado el formulario por GET o si hay valores en $_GET
if (isset($_GET['calcular'])) {
    // Obtener los valores del formulario, si no están definidos, establecerlos en 0
    $num1 = $_GET['num1'] ?? 0;
    $num2 = $_GET['num2'] ?? 0;

    // Realizar operaciones matemáticas básicas
    $suma = $num1 + $num2;
    $resta = $num1 - $num2;
    $multiplicacion = $num1 * $num2;

    // Verificar la división por cero antes de realizar la operación
    $division = ($num2 != 0) ? $num1 / $num2 : "No se puede dividir por cero";

    // Crear una cadena de resultados con formato HTML, sin incluir un título
    $resultados = "
        <p>$num1 + $num2 = $suma</p>
        <p>$num1 - $num2 = $resta</p>
        <p>$num1 * $num2 = $multiplicacion</p>
        <p>$num1 / $num2 = $division</p>
    ";

    // Mostrar los resultados
    $mostrarResultados = true;
}
?>

<h1>Mi Calculadora</h1>

<!-- Formulario para ingresar números -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="get">
    <label for="num1">Número 1:</label>
    <input type="text" id="num1" name="num1" required>

    <br>

    <label for="num2">Número 2:</label>
    <input type="text" id="num2" name="num2" required>

    <br>

    <button type="submit" name="calcular">Calcular</button>
</form>

<!-- Mostrar los resultados solo si se hizo clic en el botón "Calcular" -->
<?php if ($mostrarResultados) echo $resultados; ?>

</body>
</html>
